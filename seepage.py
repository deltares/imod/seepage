# Calculate seepage volume
# usage: python seepage.py [seepage.yaml]
# Martijn Visser, Deltares, 2019-02

import argparse
import yaml
import imod
import pathlib
import pandas as pd
import xarray as xr

parser = argparse.ArgumentParser(description="Calculate seepage volume.")
parser.add_argument(
    "configfile",
    default="seepage.yaml",
    type=str,
    nargs="?",
    help="Path to the YAML configuration file (default: seepage.yaml)",
)
args = parser.parse_args()

with open(args.configfile) as f:
    d = yaml.safe_load(f)


def get_area(da):
    """Cell area of a dataarray, as a float (constant) or dataarray"""
    if "res" in da.attrs:
        dx, dy = da.attrs["res"]
        return abs(dx * dy)  # float
    elif ("dx" in da.coords) and ("dy" in da.coords):
        dx = da.coords["dx"]
        dy = da.coords["dy"]
        return abs(dx * dy)  # 2D dataarray
    else:
        raise ValueError(f"Cannot get cell area")


layer_top = layer_top = int(d["layer"])
layer_bottom = layer_top + 1
startdate = pd.to_datetime(d["startdate"])
enddate = pd.to_datetime(d["enddate"])

c = imod.idf.load(d["vertical_resistance"])
dtm = imod.idf.load(d["terrain_elevation"])
seeplevel = imod.idf.load(d["seepage_elevation"])
print("Loading heads, please wait")
head = imod.idf.load(d["head"])
print(f"Finished loading heads")
# select only the head data we need
head_time = head.sel(time=slice(startdate, enddate))
head_top = head_time.sel(layer=layer_top)
head_bottom = head_time.sel(layer=layer_bottom)

# Reindex the 2D IDFs that may be given at a higher resolution / bigger area.
# It will work without doing this, but now all output IDFs will have
# the same extent and resolution.
# The .load() here will speed up the seepage calculation a bit
c = c.reindex_like(head_top, method="nearest").load()
dtm = dtm.reindex_like(head_top, method="nearest")
seeplevel = seeplevel.reindex_like(head_top, method="nearest")

# set up calculations
area = get_area(c)
seepage = (head_bottom - head_top) * area / c
seepage_mean = seepage.mean(dim="time")
seepage_depth = dtm - seeplevel

# assign layers, such that they appear in the output filenames
seepage.coords["layer"] = layer_top
seepage_mean.coords["layer"] = layer_top
seepage_depth.coords["layer"] = layer_top
seeplevel.coords["layer"] = layer_top

# create the output directory if needed
seepage_path = d["seepage_output"]
seepage_elevation_path = d["seepage_elevation_output"]
seepage_depth_path = d["seepage_depth_output"]
pathlib.Path(seepage_path).parent.mkdir(parents=True, exist_ok=True)
pathlib.Path(seepage_elevation_path).parent.mkdir(parents=True, exist_ok=True)
pathlib.Path(seepage_depth_path).parent.mkdir(parents=True, exist_ok=True)

print(f"Calculating and writing results to {seepage_path}, please wait")
# since the calculation is lazy, it will only be done when writing the output
imod.idf.save(seepage_path, seepage)
imod.idf.save(seepage_path + "-mean", seepage_mean)
imod.idf.save(seepage_elevation_path, seeplevel)
imod.idf.save(seepage_depth_path, seepage_depth)
print(f"Finished writing results")
