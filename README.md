This is a Python command line tool to calculate the seepage volume from a set of iMOD Data Files (IDFs).

It can be run like this:
```
python seepage.py [seepage.yaml]
```

See the provided `seepage.yaml` which lists all available options.

This script can be used as an example of using `imod.py` to build a simple command line interface.
